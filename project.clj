(defproject blog "0.1.0-SNAPSHOT"
  :description "Microblogging application"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [;; base
                 [org.clojure/clojure "1.8.0"]
                 [org.clojure/clojurescript "1.8.51"]
                 [com.stuartsierra/component "0.3.1"]
                 [com.taoensso/timbre "4.3.1"]
                 [aprint "0.1.3"]

                 ;; backend
                 [metosin/compojure-api "1.1.0"]
                 [metosin/ring-http-response "0.6.5"]
                 [http-kit "2.1.19"]
                 [hiccup "1.0.5"]

                 ;; auth
                 [buddy "0.11.0"]

                 ;; database
                 [org.clojure/java.jdbc "0.4.2"]
                 [org.postgresql/postgresql "9.4.1208.jre7"]
                 [hikari-cp "1.6.1"]
                 [org.flywaydb/flyway-core "4.0"]
                 [honeysql "0.6.3"]

                 ;; frontend
                 [reagent "0.5.1"]
                 [re-frame "0.7.0"]
                 [secretary "1.2.3"]
                 [cljs-ajax "0.5.4"]

                 [org.webjars.bower/bootstrap "3.3.6"]]

  :source-paths ["src/clj"]
  :test-paths ["test/clj"]
  :resource-paths ["resources"]

  :uberjar-name "blog.jar"
  :main blog.main
  :aot [blog.main]

  :figwheel {:http-server-root "public"
             :css-dirs ["target/generated/public/css"]
             :nrepl-host "0.0.0.0"
             :nrepl-port 7888
             :repl false}
  
  :profiles {:dev {:repl-options {:init-ns user}
                   :resource-paths ["target/generated/js-dev" "target/generated"]
                   :dependencies [[reloaded.repl "0.2.1"]
                                  [javax.servlet/servlet-api "2.5"]]
                   :plugins [[cider/cider-nrepl "0.11.0"]
                             [lein-figwheel "0.5.3"]
                             [lein-pdo "0.1.1"]
                             [lein-ring "0.9.7"]
                             [deraen/lein-less4j "0.5.0"]]
                   :source-paths ["src" "dev-src"]
                   :ring {:reload-paths ["src"]}}}

  :cljsbuild {:builds [{:id "dev"
                        :source-paths ["src/cljs"]
                        :figwheel {:on-jsload "blog.ui.core/mount-root"}
                        :compiler {:main "blog.ui.core"
                                   :output-to "target/generated/js-dev/public/js/bundle.js"
                                   :output-dir "target/generated/js-dev/public/js/out"
                                   :asset-path "js/out"
                                   :source-map-timestamp true
                                   :optimizations :none}}

                       {:id "min"
                        :source-paths ["src/cljs"]
                        :compiler {:main "blog.ui.core"
                                   :output-to "target/generated/js-adv/public/js/bundle.js"
                                   :output-dir "target/generated/js-adv/public/js/out"
                                   :asset-path "js/out"
                                   :optimizations :advanced
                                   :pretty-print false}}]}

  :less {:source-paths ["src/less"]
         :target-path "target/generated/public/css"}
  
  :aliases {"migrate" ["run" "-m" "blog.system/migrate-system"]
            "develop" ["pdo" ["less4j" "auto"] ["figwheel" "dev"]]})
