CREATE TABLE blog.posts (
       id serial PRIMARY KEY,
       content varchar(180),
       created timestamp with time zone DEFAULT current_timestamp,
       created_by integer REFERENCES blog.users(id)
);
