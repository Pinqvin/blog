CREATE SCHEMA blog;

CREATE TABLE blog.users (
       id serial PRIMARY KEY,
       username text UNIQUE,
       password varchar(100),
       email text
);
