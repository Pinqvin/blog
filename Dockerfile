FROM pottava/clojure:1.8

RUN mkdir /server
WORKDIR /server

ADD project.clj /server/

RUN lein deps

ADD . /server/
