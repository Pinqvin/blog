(ns blog.ui.core
  (:require [re-frame.core :as re-frame]
            [reagent.core :as reagent]
            blog.ui.handlers
            blog.ui.subs
            [blog.ui.views :refer [main-panel]]))

(enable-console-print!)

(defn mount-root []
  (reagent/render [main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (re-frame/dispatch-sync [:initialize-db])
  (mount-root))
