(ns blog.ui.handlers
  (:require [re-frame.core :as re-frame]
            [blog.ui.db :as db]
            [ajax.core :refer [GET transit-request-format]]))

(re-frame/register-handler
  :initialize-db
  (fn [_ _]
    db/default-db))

(re-frame/register-handler
  :get-messages
  (fn [db _]
    (GET
        "/api/blogs"
        {:handler #(re-frame/dispatch [:process-messages %1])
         :error-handler #(re-frame/dispatch [:message-error-handler %1])
         :response-format :transit
         :keywords? true})
    (-> db
        (assoc :loading-messages? true))))

(re-frame/register-handler
  :process-messages
  (fn [db [_ response]]
    (-> db
        (assoc :loading-messages? false)
        (assoc :messages (:posts response)))))

(re-frame/register-handler
  :message-error-handler
  (fn [db [_ error]]
    (println error)
    db))
