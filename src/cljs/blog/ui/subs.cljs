(ns blog.ui.subs
  (:require-macros [reagent.ratom :refer [reaction]])
  (:require [re-frame.core :refer [register-sub]]))

(register-sub
  :loading-messages?
  (fn [db]
    (reaction (:loading-images? @db))))

(register-sub
  :messages
  (fn [db]
    (reaction (:messages @db))))
