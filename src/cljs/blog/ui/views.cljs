(ns blog.ui.views
  (:require [re-frame.core :as re-frame]))

(defn message-panel []
  (let []
    [:div.col-md-6]))

(defn messages [messages]
  [:div.col-md-6
   [:ul.blog__list
    (doall
      (for [message messages]
        ^{:key (:id message)}
        [:li.blog__message
         [:p (:content message)]
         [:small (:username message)]
         [:small.pull-right (:created message)]]))]])

(defn main-panel []
  (let [messages (re-frame/subscribe [:messages])]
    [:div.row
     [message-panel]
     [messages @messages]]))
