(ns blog.restructure
  (:require [compojure.api.meta :refer [restructure-param]]
            [blog.authentication :refer [wrap-rule]]))

(defmethod restructure-param :auth-rules
  [_ rule acc]
  (update-in acc [:middleware] conj [wrap-rule rule]))

(defmethod restructure-param :current-user
  [_ binding acc]
  (update-in acc [:letks] into [binding `(:identity ~'+compojure-api-request+)]))
