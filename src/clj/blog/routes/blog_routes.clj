(ns blog.routes.blog-routes
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer [ok]]
            [schema.core :as s]
            [blog.authentication :refer [authenticated]]
            [blog.services.blog-service :as service]
            [blog.domain.blog-domain :as domain]))

(defn blog-routes [db]
  (context "/blogs" []
    :tags ["blogs"]
    (GET "/" []
      :summary "Returns a list of blog messages"
      (ok (service/get-blog-posts db)))
    (POST "/" []
      :body [blog domain/NewBlogPost]
      :auth-rules authenticated
      :current-user user
      :summary "Create a new blog message"
      (ok (service/create-blog-post! db blog user)))
    (context "/:id" []
      :path-params [id :- s/Int]
      (GET "/" []
        :summary "Return a single blog post"
        (ok (service/get-blog-by-id db id)))
      (PUT "/" []
        :auth-rules authenticated
        :body [blog domain/UpdatedBlogPost]
        :summary "Update an existing blog post"
        (ok (service/update-blog! db id blog)))
      (DELETE "/" []
        :auth-rules authenticated
        :summary "Delete a blog post"
        (ok (service/delete-blog! db id))))))
