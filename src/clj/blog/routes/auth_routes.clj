(ns blog.routes.auth-routes
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer [ok]]
            [blog.services.auth-service :as service]
            [blog.domain.auth-domain :as domain]))

(defn auth-routes [db env]
  (context "/auth" []
    :tags ["auth"]
    (POST "/" []
      :body [login domain/LoginDetails]
      :summary "Login with username and password"
      (service/authenticate-user db env login))))
