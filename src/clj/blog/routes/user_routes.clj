(ns blog.routes.user-routes
  (:require [compojure.api.sweet :refer :all]
            [schema.core :as s]
            [ring.util.http-response :refer [ok]]
            [blog.services.user-service :as service]
            [blog.domain.user-domain :as domain]))

(defn user-routes [db]
  (routes
    (context "/users" []
      :tags ["user"]
      (GET "/" []
        :summary "Return a list of users"
        (ok (service/get-users db)))
      (POST "/" []
        :body [user domain/NewUser]
        :summary "Create a new user"
        (ok (service/create-user! db user))))))
