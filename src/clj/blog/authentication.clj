(ns blog.authentication
  (:require [buddy.auth :refer [authenticated?]]
            [buddy.auth.accessrules :refer [wrap-access-rules]]
            [ring.util.http-response :refer [unauthorized]]))

(defn authenticated [req]
  (authenticated? req))

(defn access-error [req val]
  (unauthorized val))

(defn wrap-rule [handler rule]
  (-> handler
      (wrap-access-rules {:rules [{:pattern #".*"
                                   :handler rule}]
                          :on-error access-error})))
