(ns blog.components.flyway
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log])
  (:import [org.flywaydb.core Flyway]))

(defrecord CheckMigration []
  component/Lifecycle
  (start [this]
    (let [flyway (doto (Flyway.)
                   (.setDataSource (-> (:db this)
                                       :db
                                       :datasource)))]
      (when-let [pending-migrations (-> (.info flyway)
                                        .pending
                                        seq)]
        (log/errorf "%d pending migration(s)!" (count pending-migrations)))
      this))
  (stop [this]
    this))

(defrecord DoMigration []
  component/Lifecycle
  (start [this]
    (let [flyway (doto (Flyway.)
                   (.setDataSource (-> (:db this)
                                       :db
                                       :datasource)))]
      (when-let [migrations (.migrate flyway)]
        (log/infof "Succesfully applied %d migration(s)" migrations))
      this))
  (stop [this]
    this))
