(ns blog.components.http-kit
  (:require [com.stuartsierra.component :as component]
            [org.httpkit.server :as httpkit]
            [buddy.auth.backends.token :refer [jws-backend]]
            [buddy.auth.middleware :refer [wrap-authentication]]
            [blog.handler :refer [create]]))

(defrecord HttpKit [env]
  component/Lifecycle
  (start [this]
    (assoc this :http-kit (httpkit/run-server
                            (-> (create (-> this :db :db) env)
                                (wrap-authentication (jws-backend {:secret (-> env :blog :token-secret)})))
                            (:http-kit env))))
  (stop [this]
    (if-let [http-kit (:http-kit this)]
      (http-kit))
    (dissoc this :http-kit)))
