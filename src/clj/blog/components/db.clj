(ns blog.components.db
  (:require [com.stuartsierra.component :as component]
            [hikari-cp.core :refer :all]
            [clojure.java.jdbc :as jdbc]))

(defrecord Db [settings]
  component/Lifecycle
  (start [this]
    (assoc this :db {:datasource (make-datasource settings)}))

  (stop [this]
    (if-let [db (:db this)]
      (do (close-datasource (:datasource db))
          (dissoc this :db))
      this)))
