(ns blog.domain.auth-domain
  (:require [schema.core :as s]))

(s/defschema LoginDetails {:username s/Str
                           :password s/Str})
