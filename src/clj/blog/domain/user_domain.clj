(ns blog.domain.user-domain
  (:require [schema.core :as s]))

(s/defschema NewUser {:username s/Str
                      :password s/Str
                      :email s/Str})
