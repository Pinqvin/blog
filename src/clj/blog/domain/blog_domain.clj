(ns blog.domain.blog-domain
  (:require [schema.core :as s]))

(s/defschema NewBlogPost {:content s/Str})

(def UpdatedBlogPost NewBlogPost)
