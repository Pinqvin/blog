(ns blog.handler
  (:require [compojure.api.sweet :refer :all]
            [compojure.route :refer [resources]]
            [schema.core :as s]
            [ring.util.http-response :refer [ok not-found]]
            blog.restructure
            [blog.index :refer [index]]
            [blog.routes.user-routes :refer [user-routes]]
            [blog.routes.auth-routes :refer [auth-routes]]
            [blog.routes.blog-routes :refer [blog-routes]]))

(defn create [db env]
  (api
    {:swagger
     {:ui "/api-docs"
      :spec "/swagger.json"
      :data {:info {:title "Blog API"
                    :description "Blog REST API"}
             :tags [{:name "user" :description "User registration and management routes"}
                    {:name "auth" :description "Authentication routes"}
                    {:name "blogs" :description "Blog creation and management routes"}]}}}

    (GET "/" []
      :no-doc true
      (ok (index)))

    (context "/api" []
      (user-routes db)
      (auth-routes db env)
      (blog-routes db))

    (undocumented
      (resources "/" {:root "public"}))

    (ANY "*" []
      (not-found))))
