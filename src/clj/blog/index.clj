(ns blog.index
  (:require [hiccup.core :refer [html]]
            [hiccup.page :refer [html5 include-css include-js]]))

(defn index []
  (html
    (html5
      [:head
       [:meta {:charset "utf-8"}]
       [:meta {:http-equiv "X-UA-Compatible" :content "IE=edge"}]
       [:meta {:name "viewport"
               :content "width=device-width, initial-scale=1"}]
       [:title "Blog"]
       (include-css "css/styles.css")]
      [:body
       [:div#app]]
      (include-js "js/bundle.js")
      [:script "blog.ui.core.init();"])))
