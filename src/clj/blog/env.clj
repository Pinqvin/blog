(ns blog.env
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]
            [aprint.core :as aprint]))

(defn get-env []
  (let [env (merge (-> (io/resource "config-defaults.edn")
                       slurp
                       edn/read-string))]
    (aprint/aprint env)
    env))
