(ns blog.main
  (:gen-class))

(defn -main
  [& _]
  (require 'blog.system)
  ((resolve 'blog.system/new-system)))
