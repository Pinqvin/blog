(ns blog.system
  (:require [com.stuartsierra.component :as component]
            [blog.env :refer [get-env]]
            [blog.components.http-kit :as http-kit]
            [blog.components.db :as db]
            [blog.components.flyway :as flyway]))

(defn new-system []
  (let [env (get-env)]
    (component/system-map
      :db (db/->Db (:db env))
      :flyway (component/using
                (flyway/->CheckMigration)
                [:db])
      :http-kit (component/using
                  (http-kit/->HttpKit env)
                  [:db]))))

(defn migrate-system [& _]
  (let [env (get-env)]
    (-> (component/system-map
          :db (db/->Db (:db env))
          :flyway (component/using
                    (flyway/->DoMigration)
                    [:db]))
        (component/start))))
