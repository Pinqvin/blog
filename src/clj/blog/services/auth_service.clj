(ns blog.services.auth-service
  (:require [buddy.sign.jws :as jws]
            [ring.util.http-response :refer [ok bad-request]]
            [blog.services.user-service :as user]))

(defn authenticate-user [db env {:keys [username password] :as login}]
  (let [user (user/get-user-by-login db login)]
    (if user
      (ok {:token (jws/sign {:user (:id user)} (-> env :blog :token-secret))})
      (bad-request {:message "Username or password invalid"}))))
