(ns blog.services.user-service
  (:require [clojure.java.jdbc :as jdbc]
            [honeysql.core :as sql]
            [honeysql.helpers :as hsl]
            [buddy.hashers :as hashers]))

(defn get-users [db]
  (let [query (-> (hsl/select :username)
                  (hsl/from :blog.users)
                  sql/format)]
    {:users (jdbc/query db query)}))

(defn get-user-by-login [db {:keys [username password]}]
  (let [query (-> (hsl/select :id :username :password)
                  (hsl/from :blog.users)
                  (hsl/where [:= :username username])
                  sql/format)
        user (first (jdbc/query db query))]
    (if (and user (hashers/check password (:password user)))
      user)))

(defn create-user! [db {:keys [username password email]}]
  (let [query (-> (hsl/insert-into :blog.users)
                  (hsl/values [{:username username
                                :password (hashers/derive password)
                                :email email}])
                  sql/format)]
    (jdbc/execute! db query)))
