(ns blog.services.blog-service
  (:require [clojure.java.jdbc :as jdbc]
            [honeysql.core :as sql]
            [honeysql.helpers :as hsl]))

(defn get-blog-posts [db]
  (let [query (-> (hsl/select :blog.posts.id :content :created :u.username)
                  (hsl/from :blog.posts)
                  (hsl/join [:blog.users :u] [:= :created_by :u.id])
                  sql/format)
        posts (jdbc/query db query)]
    {:posts posts}))

(defn create-blog-post! [db {:keys [content]} {:keys [user]}]
  (let [query (-> (hsl/insert-into :blog.posts)
                  (hsl/columns :content :created_by)
                  (hsl/values [[content user]])
                  sql/format)]
    (jdbc/execute! db query)))

(defn get-blog-by-id [db id]
  (let [query (-> (hsl/select :blog.posts.id :content :created :u.username)
                  (hsl/from :blog.posts)
                  (hsl/join [:blog.users :u] [:= :created_by :u.id])
                  (hsl/where [:= :blog.posts.id id])
                  sql/format)]
    (jdbc/query db query)))

(defn update-blog! [db id {:keys [content]}]
  (let [query (-> (hsl/update :blog.posts)
                  (hsl/sset {:content content})
                  (hsl/where [:= :id id])
                  sql/format)]
    (jdbc/execute! db query)))

(defn delete-blog! [db id]
  (let [query (-> (hsl/delete-from :blog.posts)
                  (hsl/where [:= :id id])
                  sql/format)]
    (jdbc/execute! db query)))
