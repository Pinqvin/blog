# blog

Microblogging application (server and frontend)

## Development

To start the application through Docker, run the following commands:

```bash
docker-compose up -d db
docker-compose up -d server
```

By default, this starts a headless nREPL instance inside the docker container
that you can attach to and start the server by running the following function

```clojure
(go)
```

To build the frontend application run the following command after the server
is up:

```bash
docker-compose run -p 3449:3449 lein develop
```

## License

Copyright © 2016 Juuso Tapaninen

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
